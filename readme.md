# File Hash Calculator

## Introduction

This is a GUI application written in Python using the wx library. It allows the user to calculate the hash of a file by either entering the file path, by dragging and dropping the file into the application, or by entering the string of a pre-calculated hash to compare it with the file's hash. The user can also select the type of hash algorithm to use from a drop-down menu. The application uses multi-threading to run the hash calculation in the background. It also has a "kill thread" feature that allows the user to stop the calculation at any time. The GUI also has an "About" menu that displays the author's name when clicked.

## Features
- Calculate hash of a file by entering the file path, by dragging and dropping the file into the application, or by entering the string of a pre-calculated hash to compare it with the file's hash
- Select the type of hash algorithm to use from a drop-down menu (e.g. MD5, SHA-1, SHA-256).
- Multi-threading is used to run the hash calculation in the background.
- "Kill thread" feature allows the user to stop the calculation at any time.
- "About" menu displays the author's name when clicked.

## Requirements
- Python 3.6+
- wxPython library

## Installation
1. Clone or download the repository
2. pip install -r requirements.txt

## Usage
1. Run the application (double click on checksum_gui.py)
2. Select the file to hash by either entering the file path, by dragging and dropping the file into the application, or by entering the string of a pre-calculated hash.
3. Select the desired hash algorithm from the drop-down menu. (Default is SHA-256)
4. Click the "Calculate" button to begin the calculation.
5. Wait for the calculation to complete or use the "Kill thread" feature to stop it. (Relaunch button)
