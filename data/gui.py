import sys
import threading
import wx
import wx.html
from .hash_data import hash_types


class thread_with_trace(threading.Thread):
    """
    https://www.geeksforgeeks.org/python-different-ways-to-kill-a-thread/
        - Using traces to kill threads
    """
    def __init__(self, *args, **keywords):
        threading.Thread.__init__(self, *args, **keywords)
        self.killed = False

    def start(self):
        self.__run_backup = self.run
        self.run = self.__run
        threading.Thread.start(self)

    def __run(self):
        sys.settrace(self.globaltrace)
        self.__run_backup()
        self.run = self.__run_backup

    def globaltrace(self, frame, event, arg):
        if event == "call":
            return self.localtrace
        else:
            return None

    def localtrace(self, frame, event, arg):
        if self.killed:
            if event == "line":
                raise SystemExit()
        return self.localtrace

    def kill(self):
        self.killed = True


class MyFileDropTarget(wx.FileDropTarget):
    # https://stackoverflow.com/a/31393351
    def __init__(self, window, id):
        """Constructor"""
        wx.FileDropTarget.__init__(self)
        self.window = window
        self.id = id

    def OnDropFiles(self, x, y, filenames):
        self.window.updateText(self.id, filenames[0])
        # print(filenames)
        # for filepath in filenames:
        #     self.window.updateText(filepath + '\n')
        return True


class MyApp(wx.App):

    def __init__(self):
        super().__init__(clearSigInt=True)
        self.init_frame()

    def init_frame(self):
        frame = MyFrame(parent=None, title="Checksum", pos=(-7, 0))
        frame.Show()


class AboutMenu(wx.Menu):
    def __init__(self, parentFrame):
        super().__init__()
        self.OnInit()
        self.parentFrame = parentFrame

    def OnInit(self):
        # About
        about_option = wx.MenuItem(
            parentMenu=self,
            id=300,
            text="&About\tCtrl+F1",
            kind=wx.ITEM_NORMAL,
        )
        self.Append(about_option)
        self.Bind(wx.EVT_MENU, handler=self.on_about, source=about_option)

    def on_about(self, event):
        dlg = wx.MessageDialog(
            None,
            message="Benoit Boulanger © 2019",
            caption="Built by",
            style=wx.OK | wx.CENTRE | wx.ICON_NONE,
        )
        dlg.ShowModal()


class MyFrame(wx.Frame):

    def __init__(self, parent, title, pos):
        super().__init__(parent=parent, title=title, pos=pos)
        self.on_init()

    def on_init(self):
        menu_bar = wx.MenuBar()
        about_menu = AboutMenu(parentFrame=self)
        menu_bar.Append(about_menu, '&About')
        self.SetMenuBar(menu_bar)

        self.CreateStatusBar()
        self.SetStatusText("")

        _ = MyPanel(parent=self)
        self.Fit()


class MyPanel(wx.Panel):

    def __init__(self, parent):
        super().__init__(parent=parent)
        self.parent = parent
        self.init_panel()

    def init_panel(self):
        self.hash_label = wx.StaticText(
            self, label="Select a hashing algorithm: ", style=wx.ALIGN_RIGHT
        )
        self.hashes = [v[0] for k, v in hash_types.items()]
        self.hash_choices = wx.ComboBox(
            self, choices=self.hashes, style=wx.CB_READONLY | wx.CB_DROPDOWN
        )
        self.hash_choices.SetValue("SHA-256")  # set default value

        drag_and_drop_line1 = "Enter full file path of the files"
        drag_and_drop_line2 = (
            "(you can drag and drop files to get the full path automaticly)"
        )
        drag_and_drop_line3 = (
            "or enter the checksum to compare the file with it."
        )
        drag_and_drop_message = "\n".join(
            [drag_and_drop_line1, drag_and_drop_line2, drag_and_drop_line3]
        )
        self.drag_and_drop_label = wx.StaticText(
            self, label=drag_and_drop_message, style=wx.ALIGN_CENTER
        )
        file_drop_target = MyFileDropTarget(self, 1)
        file_drop_target2 = MyFileDropTarget(self, 2)
        self.fileTextCtrl1 = wx.TextCtrl(self, style=wx.TE_LEFT)
        self.fileTextCtrl1.SetDropTarget(file_drop_target)
        self.fileTextCtrl1_label = wx.StaticText(
            self, label="", style=wx.ALIGN_CENTER | wx.ST_NO_AUTORESIZE
        )

        self.fileTextCtrl2 = wx.TextCtrl(self, style=wx.TE_LEFT)
        self.fileTextCtrl2.SetDropTarget(file_drop_target2)
        self.fileTextCtrl2_label = wx.StaticText(
            self, label="", style=wx.ALIGN_CENTER | wx.ST_NO_AUTORESIZE
        )

        self.result_label_mess = "Click the button bellow to compare"
        self.result_label = wx.TextCtrl(
            self,
            value=self.result_label_mess,
            style=wx.TE_CENTER | wx.TE_READONLY,
        )
        self.result_label.SetForegroundColour((0, 0, 0))
        self.result_label.SetBackgroundColour((191, 191, 191))

        self.get_hash_btn = wx.Button(self, wx.ID_ANY, "Generate/Diff Hashes")
        self.reload_btn = wx.Button(self, wx.ID_ANY, "Relaunch")
        self.quit_btn = wx.Button(self, wx.ID_ANY, "Quit")
        self.Bind(wx.EVT_BUTTON, self.on_get_hash, self.get_hash_btn)
        self.Bind(wx.EVT_BUTTON, self.on_reload, self.reload_btn)
        self.Bind(wx.EVT_BUTTON, self.on_quit, self.quit_btn)

        self.sizers()
        self.launch_app()

    def kill_threads(self):
        """
        kill any previously launched thread
          to prevent multiple non main threads to run simultaneously
        """
        for thread in threading.enumerate():
            if thread._name != "MainThread":
                thread.kill()
                print("thread killed")  # DEBUG

    def launch_app(self):
        self.kill_threads()

        t = thread_with_trace(target=self.worker, args=(self, self.parent))
        t.daemon = True
        t.start()

    def updateText(self, target, text):
        """
        Write text to the text control
        """
        if target == 1:
            current_target = self.fileTextCtrl1
        elif target == 2:
            current_target = self.fileTextCtrl2

        current_target.Clear()
        current_target.WriteText(text)

    def on_get_hash(self, event):
        self.kill_threads()
        self.result_label.SetLabel("Calculating checksum...")
        self.result_label.SetForegroundColour((0, 0, 0))
        self.result_label.SetBackgroundColour((191, 191, 191))

        t = thread_with_trace(
            target=self.get_hash,
            args=(
                self,
                self.hash_choices.GetValue(),
                self.fileTextCtrl1.GetValue(),
                self.fileTextCtrl2.GetValue(),
            ),
            daemon=True,
        )
        t.start()

    def on_reload(self, event):
        self.fileTextCtrl1.SetLabel("")
        self.fileTextCtrl1_label.SetLabel("")
        self.fileTextCtrl2.SetLabel("")
        self.fileTextCtrl2_label.SetLabel("")

        self.result_label.SetLabel(self.result_label_mess)
        self.result_label.SetForegroundColour((0, 0, 0))
        self.result_label.SetBackgroundColour((191, 191, 191))

        self.launch_app()

    def on_quit(self, event):
        self.parent.Close()

    def sizers(self):
        main_sizer = wx.BoxSizer(wx.VERTICAL)
        hash_choices_sizer = wx.BoxSizer(wx.HORIZONTAL)
        drag_and_drop_label_sizer = wx.BoxSizer(wx.HORIZONTAL)
        drag_and_drop_sizer = wx.BoxSizer(wx.HORIZONTAL)
        drag_and_drop_label1_sizer = wx.BoxSizer(wx.HORIZONTAL)
        drag_and_drop_sizer2 = wx.BoxSizer(wx.HORIZONTAL)
        drag_and_drop_label2_sizer = wx.BoxSizer(wx.HORIZONTAL)
        result_label_sizer = wx.BoxSizer(wx.HORIZONTAL)
        main_btn_sizer = wx.BoxSizer(wx.HORIZONTAL)
        btn_sizer = wx.BoxSizer(wx.HORIZONTAL)

        hash_choices_sizer.Add(self.hash_label, 2, wx.ALL, 7)
        hash_choices_sizer.Add(self.hash_choices, 1, wx.ALL, 5)
        drag_and_drop_label_sizer.Add(self.drag_and_drop_label, 1, wx.ALL, 5)
        drag_and_drop_sizer.Add(self.fileTextCtrl1, 1, wx.ALL, 5)
        drag_and_drop_label1_sizer.Add(
            self.fileTextCtrl1_label, 1, wx.ALL, -15
        )
        drag_and_drop_sizer2.Add(self.fileTextCtrl2, 1, wx.ALL, 5)
        drag_and_drop_label2_sizer.Add(
            self.fileTextCtrl2_label, 1, wx.ALL, -15
        )
        result_label_sizer.Add(self.result_label, 1, wx.ALL, 5)
        main_btn_sizer.Add(self.get_hash_btn, 1, wx.ALL, 5)
        btn_sizer.Add(self.reload_btn, 2, wx.ALL, 5)
        btn_sizer.Add(self.quit_btn, 1, wx.ALL, 5)

        main_sizer.Add(hash_choices_sizer, 0, wx.ALL | wx.EXPAND, 5)
        main_sizer.Add(wx.StaticLine(self), 0, wx.ALL | wx.EXPAND, 5)
        main_sizer.Add(drag_and_drop_label_sizer, 0, wx.ALL | wx.EXPAND, 5)
        main_sizer.Add(drag_and_drop_sizer, 0, wx.ALL | wx.EXPAND, 5)
        main_sizer.Add(drag_and_drop_label1_sizer, 0, wx.ALL | wx.EXPAND, 5)
        main_sizer.Add(drag_and_drop_sizer2, 0, wx.ALL | wx.EXPAND, 5)
        main_sizer.Add(drag_and_drop_label2_sizer, 0, wx.ALL | wx.EXPAND, 5)
        main_sizer.Add(wx.StaticLine(self), 0, wx.ALL | wx.EXPAND, 5)
        main_sizer.Add(result_label_sizer, 0, wx.ALL | wx.EXPAND, 5)
        main_sizer.Add(main_btn_sizer, 0, wx.ALL | wx.EXPAND, 5)
        main_sizer.Add(btn_sizer, 0, wx.ALL | wx.EXPAND, 5)

        self.SetSizer(main_sizer)
        main_sizer.Fit(self)
        self.Layout()

    def worker(self, panel=None, frame=None):
        pass

    def calculate_hash(self, file_path, hash_type):
        file_path = file_path.strip('"')
        hash = hash_type()
        with open(file_path, "rb") as f:
            # read and update hash string value in blocks of 4K
            for byte_block in iter(lambda: f.read(4096), b""):
                hash.update(byte_block)
            readable_hash = hash.hexdigest()
        return readable_hash

    def set_label(self, panel, label, target):
        if target == 1:
            panel.fileTextCtrl1_label.SetLabel(label)
        elif target == 2:
            panel.fileTextCtrl2_label.SetLabel(label)

    def process_data(self, panel, hash_type, value, target):
        if "\\" not in value and "/" not in value:
            # not a filepath, I assume it's a hash
            self.set_label(panel, value, target)
        else:
            readable_hash = self.calculate_hash(value, hash_type)
            self.set_label(panel, readable_hash, target)

    def get_hash_type(self, hash_name):
        hash_type = [v[1] for k, v in hash_types.items() if v[0] == hash_name]
        return hash_type[0]

    def get_hash(self, panel, hash_name, value1, value2):
        hash_type = self.get_hash_type(hash_name)
        print(value1)
        print(value2)
        threads = []
        t1 = thread_with_trace(
            target=self.process_data,
            args=(panel, hash_type, value1, 1),  # target id
            daemon=True,
        )
        t2 = thread_with_trace(
            target=self.process_data,
            args=(panel, hash_type, value2, 2),  # target id
            daemon=True,
        )
        t1.start()
        t2.start()
        threads.append(t1)
        threads.append(t2)
        for t in threads:
            t.join()
        print("Here's the hash!")
        label1 = panel.fileTextCtrl1_label.GetLabel().lower()
        label2 = panel.fileTextCtrl2_label.GetLabel().lower()
        if label1 == label2:
            panel.result_label.SetLabel("Same")
            panel.result_label.SetForegroundColour((0, 0, 0))
            panel.result_label.SetBackgroundColour((0, 200, 0))
        else:
            panel.result_label.SetLabel("Different")
            panel.result_label.SetForegroundColour((0, 0, 0))
            panel.result_label.SetBackgroundColour((255, 200, 75))
