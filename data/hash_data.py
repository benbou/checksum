import hashlib


# https://docs.python.org/3/library/hashlib.html#hash-algorithms
hash_types = {
    1: ("MD5", hashlib.md5),
    2: ("SHA-1", hashlib.sha1),
    3: ("SHA-224", hashlib.sha224),
    4: ("SHA-256", hashlib.sha256),
    5: ("SHA-384", hashlib.sha384),
    6: ("SHA-512", hashlib.sha512),
    7: ("Blake-2b", hashlib.blake2b),
    8: ("Blake-2s", hashlib.blake2s),
    9: ("SHA3-224", hashlib.sha3_224),
    10: ("SHA3-256", hashlib.sha3_256),
    11: ("SHA3-384", hashlib.sha3_384),
    12: ("SHA3-512", hashlib.sha3_512),
    13: ("Shake-128", hashlib.shake_128),
    14: ("Shake-256", hashlib.shake_256),
}
