from data.hash_data import hash_types


def hash_select():
    print("Select the hash to use, default is SHA-256.")
    print()
    for k, v in hash_types.items():
        print(f"{k}: {v[0]}")
    while True:
        try:
            hash_id = input("Enter a number: ")
            if hash_id == "":  # use default
                return hash_types[4]
            hash_id = int(hash_id)
            if hash_id < 1 or hash_id > 14:
                print(
                    "Invalid input, you must enter a number between 1 and 14."
                )
                continue
            return hash_types[hash_id]
        except ValueError:
            print("Invalid input.")
            continue


hash_type = hash_select()
print()
string_validation = input(
    f"Enter the {hash_type[0]} Checksum provided on the website "
    f"(or leave empty for files diff): ")
file_path1 = input("Enter the full path of the file to validate: ")
if string_validation == "":
    file_path2 = input("Enter the full path of the 2nd file to validate: ")


def get_hash(file_path, hash_type):
    file_path = file_path.strip('"')
    hash = hash_type()
    with open(file_path, "rb") as f:
        # read and update hash string value in blocks of 4K
        for byte_block in iter(lambda: f.read(4096), b""):
            hash.update(byte_block)
        readable_hash = hash.hexdigest()
    return readable_hash


readable_hash = get_hash(file_path1, hash_type[1])
if string_validation == "":
    string_validation = get_hash(file_path2, hash_type[1])


print()
print(f"{string_validation}")
print(f"{readable_hash}")
print()
print(
    "Does the hash match: {}".format(
        readable_hash.upper() == string_validation.upper()
    )
)
