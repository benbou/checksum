import ctypes
from data.gui import MyApp


def main():
    app = MyApp()
    app.MainLoop()


if __name__ == "__main__":
    ctypes.windll.user32.ShowWindow(
        ctypes.windll.kernel32.GetConsoleWindow(), 0
    )
    main()
