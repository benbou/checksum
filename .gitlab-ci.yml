---
variables:
  ARTIFACT_DIR: "bin"  # directory where the artifacts are generated
  RELEASE_NAME: "Checksum"
  RELEASE_DESCRIPTION: "Release $CI_COMMIT_TAG"
  URL1: "https://gitlab.com/benbou/checksum/-/jobs/${BWE_JOB_ID}/"
  URL2: "artifacts/raw/bin/${RELEASE_NAME}.exe"
  ARTIFACT_URL: "${URL1}${URL2}"


stages:
  - validation
  - build
  - release


# =============================================================================
# VALIDATION
# =============================================================================

yaml_linting:
  stage: validation
  image: sdesbure/yamllint
  script:
    - yamllint -s .gitlab-ci.yml


python_linting:
  stage: validation
  image: pipelinecomponents/flake8:latest
  script:
    - flake8 *.py


# =============================================================================
# ARTIFACTS
# =============================================================================

build_windows_exe:
  stage: build
  image:
    name: cdrx/pyinstaller-windows
    entrypoint: [""]
  before_script:
    - echo $CI_JOB_ID
    # Writing BWE_JOB_ID variable to env file, the value is needed for release.
    - echo BWE_JOB_ID=$CI_JOB_ID >> build_windows_exe.env
  script:
    - rm -rfv "./${ARTIFACT_DIR}"
    - python -V
    - pip -V
    - pip install -r requirements.txt
    - >-
      pyinstaller -F "checksum_gui.py"
      --icon="icon/checksum.ico"
      --name="${RELEASE_NAME}"
    - mkdir "${ARTIFACT_DIR}"
    - mv -v ./dist/* "./${ARTIFACT_DIR}/"
  artifacts:
    paths:
      - ./bin/*
    expire_in: 1 week
    name: "build-$CI_COMMIT_REF_SLUG"
    reports:
      # To ensure we've access to this file in the next stage
      dotenv: build_windows_exe.env
  only:
    - master
    - tags

# https://hub.docker.com/r/cdrx/pyinstaller-windows/dockerfile
# https://github.com/cdrx/docker-pyinstaller


# =============================================================================
# RELEASE
# =============================================================================

create_release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  script:
    - echo "running create_release"
    - echo "Previous Job ID is printed below"
    - echo $BWE_JOB_ID
  needs:
    - job: build_windows_exe
      artifacts: true
  release:
    name: Release Executables $CI_COMMIT_TAG
    description: ${RELEASE_DESCRIPTION}
    # tag_name is a mendatory field and can not be an empty string
    tag_name: $CI_COMMIT_TAG
    assets:
      links:
        - name: Windows executable
          url: $ARTIFACT_URL
  only:
    - tags
